# Fedora Atomic Desktops SIG Issue Tracker

Issue tracker for the Fedora Atomic Desktops SIG.

While each variant has it's own issue tracker, we gather here the issues that
impact all Atomic desktops in Fedora.

Please file desktop specific issues in their respective issue trackers.

## Desktop specific issue trackers

- [Silverblue](https://github.com/fedora-silverblue/issue-tracker/issues?q=is%3Aissue+is%3Aopen+sort%3Aupdated-desc)
  - See also the [Workstation issue tracker](https://pagure.io/fedora-workstation/issues?status=Open&order_key=last_updated&order=desc)
- [Kinoite](https://pagure.io/fedora-kde/SIG/issues?status=Open&order_key=last_updated&order=desc)
  ([KDE SIG](https://fedoraproject.org/wiki/SIGs/KDE))
- [Sway Atomic](https://gitlab.com/fedora/sigs/sway/SIG/-/issues)
  ([Sway SIG](https://fedoraproject.org/wiki/SIGs/Sway))
- [Budgie Atomic](https://pagure.io/fedora-budgie/project/issues?status=Open&order_key=last_updated&order=desc)
  ([Budgie SIG](https://fedoraproject.org/wiki/SIGs/Budgie))

## Meeting notes

Meetings notes for the Fedora Atomic Desktops SIG are available in the
[meetings](meetings) folder.
